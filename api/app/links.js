const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Link = require('../models/Link');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },

  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const query = {};
    const sort = {};

    const links = await Link.find(query).sort(sort);

    return res.send(links);
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const linkData = {
      shortUrl: nanoid(4),
      originalUrl: req.body.originalUrl,
    };

    const link = new Link(linkData);
    await link.save();

    return res.send(JSON.stringify(linkData.shortUrl))
  } catch (e) {
    next(e);
  }
});

router.get('/:shortUrl', async (req, res) =>{
  const shortUrl = await Link.findOne({shortUrl: req.params.shortUrl});
  if(shortUrl === null){
    return res.status(404).send({message: 'Not found!'});
  } else{
    return res.status(301).redirect(shortUrl.originalUrl);

  }

});

module.exports = router;

