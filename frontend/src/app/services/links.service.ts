import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LinkData } from '../models/link.model';


@Injectable({
  providedIn: 'root'
})
export class LinksService {

  constructor(private http: HttpClient) { }

  getShortLinks(shortUrl: string) {
    return this.http.get('http://localhost:8000/links' + shortUrl);
  }

  createLink(linkData: LinkData){
    return this.http.post('http://localhost:8000/links', linkData);
  }
}
