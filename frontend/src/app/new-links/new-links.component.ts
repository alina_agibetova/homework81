import { Component, OnInit, ViewChild } from '@angular/core';
import { LinksService } from '../services/links.service';
import { LinkData } from '../models/link.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-new-links',
  templateUrl: './new-links.component.html',
  styleUrls: ['./new-links.component.sass']
})
export class NewLinksComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  shortUrl!: any;

  constructor(private linksService: LinksService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const linkData: LinkData = this.form.value;
    this.linksService.createLink(linkData).subscribe(shortUrl => {
      this.shortUrl = shortUrl;
    })
  }

  onClick() {
    this.linksService.getShortLinks(this.shortUrl).subscribe();
  }
}
